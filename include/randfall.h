#ifndef RANDFALL_H
#define RANDFALL_H

#include <stdbool.h>
#include <wayland-client-core.h>
#include <wayland-client-protocol.h>
#include <wayland-util.h>

#include "presentation-time-protocol.h"
#include "single-pixel-buffer-v1-protocol.h"
#include "viewporter-protocol.h"
#include "wlr-layer-shell-unstable-v1-protocol.h"
#include "xdg-foreign-unstable-v1-protocol.h"
#include "xdg-foreign-unstable-v2-protocol.h"
#include "xdg-shell-protocol.h"

struct rf_ctx {
	struct wl_display *wl_display;
	struct wl_registry *wl_registry;
};

extern struct rf_ctx rf_ctx;

struct rf_buffer {
	struct wl_buffer *wl_buffer;
	int32_t width, height;
	int32_t size;
	void *data;

	void (*release_handler_func)(void *data);
	void *release_handler_data;
};

struct rf_image_data {
	int width;
	int height;
	char data[600 * 400 * 3 + 1];
};

extern const struct rf_image_data rf_image_data;

struct rf_global {
	const char *name;
	const struct wl_interface *interface;

	void **raw_global_ptr;

	uint32_t min_version;
	uint32_t latest_version;

	struct wl_list link;
};

#define RF_GLOBAL_DECLARE_TYPE(INTERFACE) \
	struct rf_g_##INTERFACE { \
		struct rf_global base; \
		union { \
			struct INTERFACE *global; \
			void *raw_global; \
		}; \
	}

#define RF_GLOBAL_DECLARE_SHARED(INTERFACE) \
	RF_GLOBAL_DECLARE_TYPE(INTERFACE); \
\
	extern struct rf_g_##INTERFACE rf_g_##INTERFACE

#define RF_GLOBAL_DEFINE(INTERFACE, NAME) \
	struct rf_g_##INTERFACE NAME = { \
		.base = \
				{ \
					.name = #INTERFACE, \
					.interface = &INTERFACE##_interface, \
					.raw_global_ptr = &NAME.raw_global, \
				}, \
	}

RF_GLOBAL_DECLARE_SHARED(wl_shm);
RF_GLOBAL_DECLARE_SHARED(wl_compositor);
RF_GLOBAL_DECLARE_SHARED(wl_subcompositor);
RF_GLOBAL_DECLARE_SHARED(wl_data_device_manager);

RF_GLOBAL_DECLARE_SHARED(wp_presentation);

RF_GLOBAL_DECLARE_SHARED(wp_viewporter);

RF_GLOBAL_DECLARE_SHARED(xdg_wm_base);

RF_GLOBAL_DECLARE_SHARED(wp_single_pixel_buffer_manager_v1);

RF_GLOBAL_DECLARE_SHARED(zwlr_layer_shell_v1);

RF_GLOBAL_DECLARE_SHARED(zxdg_exporter_v1);
RF_GLOBAL_DECLARE_SHARED(zxdg_importer_v1);

RF_GLOBAL_DECLARE_SHARED(zxdg_exporter_v2);
RF_GLOBAL_DECLARE_SHARED(zxdg_importer_v2);

RF_GLOBAL_DECLARE_TYPE(wl_seat);
RF_GLOBAL_DECLARE_TYPE(wl_output);

struct rf_wl_keyboard {
	struct wl_keyboard *wl_keyboard;
	const struct wl_keyboard_listener *wl_keyboard_listener;
	void *data;
};

struct rf_wl_pointer {
	struct wl_pointer *wl_pointer;
	const struct wl_pointer_listener *wl_pointer_listener;
	void *data;
};

void rf_wl_keyboard_init(struct rf_wl_keyboard *keyboard, struct wl_seat *seat,
		const struct wl_keyboard_listener *wl_keyboard_listener, void *data);

void rf_wl_pointer_init(struct rf_wl_pointer *pointer, struct wl_seat *seat,
		const struct wl_pointer_listener *wl_pointer_listener, void *data);

struct rf_xdg_toplevel {
	struct wl_surface *wl_surface;
	struct xdg_surface *xdg_surface;
	struct xdg_toplevel *xdg_toplevel;

	const struct xdg_surface_listener *xdg_surface_listener;
	const struct xdg_toplevel_listener *xdg_toplevel_listener;

	void *data;
};

struct rf_xdg_popup {
	struct wl_surface *wl_surface;
	struct xdg_surface *xdg_surface;
	struct xdg_popup *xdg_popup;

	const struct xdg_surface_listener *xdg_surface_listener;
	const struct xdg_popup_listener *xdg_popup_listener;

	void *data;
};

void rf_xdg_toplevel_init(struct rf_xdg_toplevel *toplevel,
		const struct xdg_surface_listener *xdg_surface_listener,
		const struct xdg_toplevel_listener *xdg_toplevel_listener, void *data);

void rf_xdg_popup_init(struct rf_xdg_popup *popup, struct xdg_surface *parent,
		struct xdg_positioner *positioner, const struct xdg_surface_listener *xdg_surface_listener,
		const struct xdg_popup_listener *xdg_popup_listener, void *data);

void rf_xdg_wm_base_add_ponger(void);

struct rf_wlr_layer_surface {
	struct wl_surface *wl_surface;
	struct zwlr_layer_surface_v1 *zwlr_layer_surface_v1;

	const struct zwlr_layer_surface_v1_listener *listener;

	void *data;
};

void rf_wlr_layer_surface_init(struct rf_wlr_layer_surface *layer_surface, struct wl_output *output,
		enum zwlr_layer_shell_v1_layer layer, const char *namespace,
		const struct zwlr_layer_surface_v1_listener *listener, void *data);

struct rf_buffer *rf_buffer_create(int32_t width, int32_t height);
struct rf_buffer *rf_buffer_create_with_release_handler(int32_t width, int32_t height,
		void (*release_handler_func)(void *data), void *release_handler_data);

void rf_buffer_fill(struct rf_buffer *buffer, uint8_t r, uint8_t g, uint8_t b);

void rf_util_abort(const char *fmt, ...) __attribute__((format(printf, 1, 2)));

void rf_ctx_start(void);

void rf_ctx_expect_success(void);
void rf_ctx_expect_error(void);

void rf_global_ensure(struct rf_global *global, uint32_t version);

void rf_global_collect_all(void);

#endif

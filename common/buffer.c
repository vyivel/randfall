#include <assert.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>
#include <wayland-client-protocol.h>

#include "randfall.h"

static void buffer_handle_release(void *data, struct wl_buffer *wl_buffer) {
	struct rf_buffer *buffer = data;
	if (buffer->release_handler_func != NULL) {
		buffer->release_handler_func(buffer->release_handler_data);
	}
	munmap(buffer->data, buffer->size);
	wl_buffer_destroy(buffer->wl_buffer);
	free(buffer);
}

static const struct wl_buffer_listener buffer_listener = {
	.release = buffer_handle_release,
};

static void fnv_1a_cont(uint64_t *h, const void *data, size_t len) {
	const uint8_t *bytes = data;
	for (size_t i = 0; i < len; i++) {
		*h = (*h * 0x00000100000001B3) ^ bytes[i];
	}
}

static void generate_name(char buffer[static 16]) {
	uint64_t h = 0xcbf29ce484222325;
	int pid = getpid();
	fnv_1a_cont(&h, &pid, sizeof(pid));
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	fnv_1a_cont(&h, &ts.tv_sec, sizeof(&ts.tv_sec));
	fnv_1a_cont(&h, &ts.tv_nsec, sizeof(&ts.tv_nsec));
	for (size_t i = 16; i-- > 0;) {
		buffer[i] = "0123456789abcdef"[h & 0xF];
		h >>= 4;
	}
}

struct rf_buffer *rf_buffer_create(int32_t width, int32_t height) {
	return rf_buffer_create_with_release_handler(width, height, NULL, NULL);
}

struct rf_buffer *rf_buffer_create_with_release_handler(int32_t width, int32_t height,
		void (*release_handler_func)(void *data), void *release_handler_data) {
	assert(rf_g_wl_shm.global != NULL);
	assert(width > 0);
	assert(height > 0);

	struct rf_buffer *buffer = calloc(1, sizeof(*buffer));
	if (buffer == NULL) {
		goto err_buffer;
	}

	uint32_t stride = width * 4;
	size_t size = stride * height;

	static const char template[] = "randfall-";
	char name[sizeof(template) + 16] = {0};
	memcpy(name, template, sizeof(template) - 1);
	generate_name(&name[sizeof(template) - 1]);

	int fd = shm_open(name, O_RDWR | O_CREAT | O_EXCL, 0600);
	if (fd < 0) {
		goto err_fd;
	}
	shm_unlink(name);

	int flags = fcntl(fd, F_GETFD);
	if (flags == -1) {
		goto err_fcntl;
	}
	if (fcntl(fd, F_SETFD, flags | FD_CLOEXEC) == -1) {
		goto err_fcntl;
	}
	if (ftruncate(fd, size) < 0) {
		goto err_ftruncate;
	}

	struct wl_shm_pool *pool = wl_shm_create_pool(rf_g_wl_shm.global, fd, size);
	if (pool == NULL) {
		goto err_pool;
	}
	void *data = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (data == MAP_FAILED) {
		goto err_mmap;
	}

	struct wl_buffer *wl_buffer =
			wl_shm_pool_create_buffer(pool, 0, width, height, stride, WL_SHM_FORMAT_ARGB8888);
	if (wl_buffer == NULL) {
		goto err_wl_buffer;
	}
	wl_buffer_add_listener(wl_buffer, &buffer_listener, buffer);

	buffer->wl_buffer = wl_buffer;
	buffer->width = width;
	buffer->height = height;
	buffer->size = size;
	buffer->data = data;

	buffer->release_handler_func = release_handler_func;
	buffer->release_handler_data = release_handler_data;

	wl_shm_pool_destroy(pool);
	close(fd);

	return buffer;

err_wl_buffer:
	munmap(data, size);
err_mmap:
	wl_shm_pool_destroy(pool);
err_pool:
err_ftruncate:
err_fcntl:
	close(fd);
err_fd:
	free(buffer);
err_buffer:
	return NULL;
}

void rf_buffer_fill(struct rf_buffer *buffer, uint8_t r, uint8_t g, uint8_t b) {
	char *data = buffer->data;
	for (int32_t i = 0; i < buffer->size; i += 4) {
		data[i + 0] = b;
		data[i + 1] = g;
		data[i + 2] = r;
		data[i + 3] = 0xFF;
	}
}

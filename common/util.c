#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "randfall.h"

void rf_util_abort(const char *fmt, ...) {
	va_list args;
	va_start(args, fmt);

	fprintf(stderr, "FATAL: ");
	vfprintf(stderr, fmt, args);
	fprintf(stderr, "\n");

	va_end(args);

	exit(1);
}

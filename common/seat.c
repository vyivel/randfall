#include <assert.h>
#include <string.h>
#include <wayland-client-protocol.h>
#include <wayland-util.h>

#include "common.h"
#include "randfall.h"

static void default_wl_keyboard_handle_keymap(
		void *data, struct wl_keyboard *wl_keyboard, uint32_t format, int32_t fd, uint32_t size) {
	struct rf_wl_keyboard *keyboard = data;
	if (keyboard->wl_keyboard_listener != NULL && keyboard->wl_keyboard_listener->keymap != NULL) {
		keyboard->wl_keyboard_listener->keymap(keyboard->data, wl_keyboard, format, fd, size);
	}
}

static void default_wl_keyboard_handle_enter(void *data, struct wl_keyboard *wl_keyboard,
		uint32_t serial, struct wl_surface *surface, struct wl_array *keys) {
	struct rf_wl_keyboard *keyboard = data;
	if (keyboard->wl_keyboard_listener != NULL && keyboard->wl_keyboard_listener->enter != NULL) {
		keyboard->wl_keyboard_listener->enter(keyboard->data, wl_keyboard, serial, surface, keys);
	}
}

static void default_wl_keyboard_handle_leave(
		void *data, struct wl_keyboard *wl_keyboard, uint32_t serial, struct wl_surface *surface) {
	struct rf_wl_keyboard *keyboard = data;
	if (keyboard->wl_keyboard_listener != NULL && keyboard->wl_keyboard_listener->leave != NULL) {
		keyboard->wl_keyboard_listener->leave(keyboard->data, wl_keyboard, serial, surface);
	}
}

static void default_wl_keyboard_handle_key(void *data, struct wl_keyboard *wl_keyboard,
		uint32_t serial, uint32_t time, uint32_t key, uint32_t state) {
	struct rf_wl_keyboard *keyboard = data;
	if (keyboard->wl_keyboard_listener != NULL && keyboard->wl_keyboard_listener->key != NULL) {
		keyboard->wl_keyboard_listener->key(keyboard->data, wl_keyboard, serial, time, key, state);
	}
}

static void default_wl_keyboard_handle_modifiers(void *data, struct wl_keyboard *wl_keyboard,
		uint32_t serial, uint32_t mods_depressed, uint32_t mods_latched, uint32_t mods_locked,
		uint32_t group) {
	struct rf_wl_keyboard *keyboard = data;
	if (keyboard->wl_keyboard_listener != NULL &&
			keyboard->wl_keyboard_listener->modifiers != NULL) {
		keyboard->wl_keyboard_listener->modifiers(keyboard->data, wl_keyboard, serial,
				mods_depressed, mods_latched, mods_locked, group);
	}
}

static void default_wl_keyboard_handle_repeat_info(
		void *data, struct wl_keyboard *wl_keyboard, int32_t rate, int32_t delay) {
	struct rf_wl_keyboard *keyboard = data;
	if (keyboard->wl_keyboard_listener != NULL &&
			keyboard->wl_keyboard_listener->repeat_info != NULL) {
		keyboard->wl_keyboard_listener->repeat_info(keyboard->data, wl_keyboard, rate, delay);
	}
}

static const struct wl_keyboard_listener default_wl_keyboard_listener = {
	.keymap = default_wl_keyboard_handle_keymap,
	.enter = default_wl_keyboard_handle_enter,
	.leave = default_wl_keyboard_handle_leave,
	.key = default_wl_keyboard_handle_key,
	.modifiers = default_wl_keyboard_handle_modifiers,
	.repeat_info = default_wl_keyboard_handle_repeat_info,
};

static void default_wl_pointer_handle_enter(void *data, struct wl_pointer *wl_pointer,
		uint32_t serial, struct wl_surface *surface, wl_fixed_t surface_x, wl_fixed_t surface_y) {
	struct rf_wl_pointer *pointer = data;
	if (pointer->wl_pointer_listener != NULL && pointer->wl_pointer_listener->enter != NULL) {
		pointer->wl_pointer_listener->enter(
				pointer->data, wl_pointer, serial, surface, surface_x, surface_y);
	}
}

static void default_wl_pointer_handle_leave(
		void *data, struct wl_pointer *wl_pointer, uint32_t serial, struct wl_surface *surface) {
	struct rf_wl_pointer *pointer = data;
	if (pointer->wl_pointer_listener != NULL && pointer->wl_pointer_listener->leave != NULL) {
		pointer->wl_pointer_listener->leave(pointer->data, wl_pointer, serial, surface);
	}
}

static void default_wl_pointer_handle_motion(void *data, struct wl_pointer *wl_pointer,
		uint32_t time, wl_fixed_t surface_x, wl_fixed_t surface_y) {
	struct rf_wl_pointer *pointer = data;
	if (pointer->wl_pointer_listener != NULL && pointer->wl_pointer_listener->motion != NULL) {
		pointer->wl_pointer_listener->motion(pointer->data, wl_pointer, time, surface_x, surface_y);
	}
}

static void default_wl_pointer_handle_button(void *data, struct wl_pointer *wl_pointer,
		uint32_t serial, uint32_t time, uint32_t button, uint32_t state) {
	struct rf_wl_pointer *pointer = data;
	if (pointer->wl_pointer_listener != NULL && pointer->wl_pointer_listener->button != NULL) {
		pointer->wl_pointer_listener->button(
				pointer->data, wl_pointer, serial, time, button, state);
	}
}

static void default_wl_pointer_handle_axis(
		void *data, struct wl_pointer *wl_pointer, uint32_t time, uint32_t axis, wl_fixed_t value) {
	struct rf_wl_pointer *pointer = data;
	if (pointer->wl_pointer_listener != NULL && pointer->wl_pointer_listener->axis != NULL) {
		pointer->wl_pointer_listener->axis(pointer->data, wl_pointer, time, axis, value);
	}
}

static void default_wl_pointer_handle_frame(void *data, struct wl_pointer *wl_pointer) {
	struct rf_wl_pointer *pointer = data;
	if (pointer->wl_pointer_listener != NULL && pointer->wl_pointer_listener->frame != NULL) {
		pointer->wl_pointer_listener->frame(pointer->data, wl_pointer);
	}
}

static void default_wl_pointer_handle_axis_source(
		void *data, struct wl_pointer *wl_pointer, uint32_t axis_source) {
}

static void default_wl_pointer_handle_axis_stop(
		void *data, struct wl_pointer *wl_pointer, uint32_t time, uint32_t axis) {
	struct rf_wl_pointer *pointer = data;
	if (pointer->wl_pointer_listener != NULL && pointer->wl_pointer_listener->axis_stop != NULL) {
		pointer->wl_pointer_listener->axis_stop(pointer->data, wl_pointer, time, axis);
	}
}

static void default_wl_pointer_handle_axis_discrete(
		void *data, struct wl_pointer *wl_pointer, uint32_t axis, int32_t discrete) {
	struct rf_wl_pointer *pointer = data;
	if (pointer->wl_pointer_listener != NULL &&
			pointer->wl_pointer_listener->axis_discrete != NULL) {
		pointer->wl_pointer_listener->axis_discrete(pointer->data, wl_pointer, axis, discrete);
	}
}

static void default_wl_pointer_handle_axis_value120(
		void *data, struct wl_pointer *wl_pointer, uint32_t axis, int32_t value120) {
	struct rf_wl_pointer *pointer = data;
	if (pointer->wl_pointer_listener != NULL &&
			pointer->wl_pointer_listener->axis_value120 != NULL) {
		pointer->wl_pointer_listener->axis_value120(pointer->data, wl_pointer, axis, value120);
	}
}

static void default_wl_pointer_handle_axis_relative_direction(
		void *data, struct wl_pointer *wl_pointer, uint32_t axis, uint32_t direction) {
	struct rf_wl_pointer *pointer = data;
	if (pointer->wl_pointer_listener != NULL &&
			pointer->wl_pointer_listener->axis_relative_direction != NULL) {
		pointer->wl_pointer_listener->axis_relative_direction(
				pointer->data, wl_pointer, axis, direction);
	}
}

static const struct wl_pointer_listener default_wl_pointer_listener = {
	.enter = default_wl_pointer_handle_enter,
	.leave = default_wl_pointer_handle_leave,
	.motion = default_wl_pointer_handle_motion,
	.button = default_wl_pointer_handle_button,
	.axis = default_wl_pointer_handle_axis,
	.frame = default_wl_pointer_handle_frame,
	.axis_source = default_wl_pointer_handle_axis_source,
	.axis_stop = default_wl_pointer_handle_axis_stop,
	.axis_discrete = default_wl_pointer_handle_axis_discrete,
	.axis_value120 = default_wl_pointer_handle_axis_value120,
	.axis_relative_direction = default_wl_pointer_handle_axis_relative_direction,
};

void rf_wl_keyboard_init(struct rf_wl_keyboard *keyboard, struct wl_seat *seat,
		const struct wl_keyboard_listener *wl_keyboard_listener, void *data) {
	memset(keyboard, 0, sizeof(*keyboard));
	keyboard->wl_keyboard = wl_seat_get_keyboard(seat);
	keyboard->wl_keyboard_listener = wl_keyboard_listener;
	wl_keyboard_add_listener(keyboard->wl_keyboard, &default_wl_keyboard_listener, keyboard);
	keyboard->data = data;
}

void rf_wl_pointer_init(struct rf_wl_pointer *pointer, struct wl_seat *seat,
		const struct wl_pointer_listener *wl_pointer_listener, void *data) {
	memset(pointer, 0, sizeof(*pointer));
	pointer->wl_pointer = wl_seat_get_pointer(seat);
	pointer->wl_pointer_listener = wl_pointer_listener;
	wl_pointer_add_listener(pointer->wl_pointer, &default_wl_pointer_listener, pointer);
	pointer->data = data;
}

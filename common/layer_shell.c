#include <assert.h>
#include <string.h>

#include "randfall.h"

static void default_wlr_layer_surface_handle_configure(void *data,
		struct zwlr_layer_surface_v1 *zwlr_layer_surface_v1, uint32_t serial, uint32_t width,
		uint32_t height) {
	struct rf_wlr_layer_surface *layer_surface = data;
	if (layer_surface->listener != NULL && layer_surface->listener->configure != NULL) {
		layer_surface->listener->configure(
				layer_surface->data, zwlr_layer_surface_v1, serial, width, height);
	}
}

static void default_wlr_layer_surface_handle_closed(
		void *data, struct zwlr_layer_surface_v1 *zwlr_layer_surface_v1) {
	struct rf_wlr_layer_surface *layer_surface = data;
	if (layer_surface->listener != NULL && layer_surface->listener->closed != NULL) {
		layer_surface->listener->closed(layer_surface->data, zwlr_layer_surface_v1);
	}
}

static const struct zwlr_layer_surface_v1_listener default_wlr_layer_surface_listener = {
	.configure = default_wlr_layer_surface_handle_configure,
	.closed = default_wlr_layer_surface_handle_closed,
};

void rf_wlr_layer_surface_init(struct rf_wlr_layer_surface *layer_surface, struct wl_output *output,
		enum zwlr_layer_shell_v1_layer layer, const char *namespace,
		const struct zwlr_layer_surface_v1_listener *listener, void *data) {
	assert(rf_g_zwlr_layer_shell_v1.global != NULL);

	memset(layer_surface, 0, sizeof(*layer_surface));
	layer_surface->wl_surface = wl_compositor_create_surface(rf_g_wl_compositor.global);
	layer_surface->zwlr_layer_surface_v1 = zwlr_layer_shell_v1_get_layer_surface(
			rf_g_zwlr_layer_shell_v1.global, layer_surface->wl_surface, output, layer, namespace);
	zwlr_layer_surface_v1_add_listener(layer_surface->zwlr_layer_surface_v1,
			&default_wlr_layer_surface_listener, layer_surface);

	layer_surface->listener = listener;
	layer_surface->data = data;
}

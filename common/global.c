#include <assert.h>
#include <string.h>
#include <wayland-client-core.h>
#include <wayland-client-protocol.h>
#include <wayland-util.h>

#include "common.h"
#include "randfall.h"

#define RF_GLOBAL_DEFINE_SHARED(INTERFACE) RF_GLOBAL_DEFINE(INTERFACE, rf_g_##INTERFACE)

RF_GLOBAL_DEFINE_SHARED(wl_shm);
RF_GLOBAL_DEFINE_SHARED(wl_compositor);
RF_GLOBAL_DEFINE_SHARED(wl_subcompositor);
RF_GLOBAL_DEFINE_SHARED(wl_data_device_manager);

RF_GLOBAL_DEFINE_SHARED(wp_presentation);

RF_GLOBAL_DEFINE_SHARED(wp_viewporter);

RF_GLOBAL_DEFINE_SHARED(xdg_wm_base);

RF_GLOBAL_DEFINE_SHARED(wp_single_pixel_buffer_manager_v1);

RF_GLOBAL_DEFINE_SHARED(zwlr_layer_shell_v1);

RF_GLOBAL_DEFINE_SHARED(zxdg_exporter_v1);
RF_GLOBAL_DEFINE_SHARED(zxdg_importer_v1);

RF_GLOBAL_DEFINE_SHARED(zxdg_exporter_v2);
RF_GLOBAL_DEFINE_SHARED(zxdg_importer_v2);

#undef RF_GLOBAL_DEFINE_SHARED

static void registry_handle_global(void *data, struct wl_registry *wl_registry, uint32_t name,
		const char *interface, uint32_t version) {
	struct rf_global *global;
	wl_list_for_each (global, &rf_common_ctx.globals, link) {
		if (strcmp(global->name, interface) != 0) {
			continue;
		}
		if (*global->raw_global_ptr != NULL) {
			// Already found
			break;
		}
		if (version > global->latest_version) {
			global->latest_version = version;
		}
		if (version >= global->min_version) {
			*global->raw_global_ptr =
					wl_registry_bind(wl_registry, name, global->interface, global->min_version);
		}
		break;
	}
}

static void registry_handle_global_remove(
		void *data, struct wl_registry *wl_registry, uint32_t name) {
	// TODO
}

static struct wl_registry_listener registry_listener = {
	.global = registry_handle_global,
	.global_remove = registry_handle_global_remove,
};

void rf_global_ensure(struct rf_global *global, uint32_t version) {
	assert(version > 0);
	assert(global->min_version == 0);
	global->min_version = version;
	wl_list_insert(rf_common_ctx.globals.prev, &global->link);
}

void rf_global_collect_all(void) {
	wl_registry_add_listener(rf_ctx.wl_registry, &registry_listener, NULL);
	wl_display_roundtrip(rf_ctx.wl_display);

	struct rf_global *global;
	wl_list_for_each (global, &rf_common_ctx.globals, link) {
		if (*global->raw_global_ptr == NULL) {
			rf_util_abort("Failed to get %s version %" PRIu32 " (latest %" PRIu32 ")", global->name,
					global->min_version, global->latest_version);
		}
	}
}

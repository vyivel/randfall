#include <assert.h>
#include <string.h>
#include <wayland-util.h>

#include "randfall.h"
#include "xdg-shell-protocol.h"

static void toplevel_default_xdg_surface_handle_configure(
		void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	struct rf_xdg_toplevel *toplevel = data;
	if (toplevel->xdg_surface_listener != NULL &&
			toplevel->xdg_surface_listener->configure != NULL) {
		toplevel->xdg_surface_listener->configure(toplevel->data, xdg_surface, serial);
	}
}

static void default_xdg_toplevel_handle_configure(void *data, struct xdg_toplevel *xdg_toplevel,
		int32_t width, int32_t height, struct wl_array *states) {
	struct rf_xdg_toplevel *toplevel = data;
	if (toplevel->xdg_toplevel_listener != NULL &&
			toplevel->xdg_toplevel_listener->configure != NULL) {
		toplevel->xdg_toplevel_listener->configure(
				toplevel->data, xdg_toplevel, width, height, states);
	}
}

static void default_xdg_toplevel_handle_close(void *data, struct xdg_toplevel *xdg_toplevel) {
	struct rf_xdg_toplevel *toplevel = data;
	if (toplevel->xdg_toplevel_listener != NULL && toplevel->xdg_toplevel_listener->close != NULL) {
		toplevel->xdg_toplevel_listener->close(toplevel->data, xdg_toplevel);
	}
}

static void default_xdg_toplevel_handle_configure_bounds(
		void *data, struct xdg_toplevel *xdg_toplevel, int32_t width, int32_t height) {
	struct rf_xdg_toplevel *toplevel = data;
	if (toplevel->xdg_toplevel_listener != NULL &&
			toplevel->xdg_toplevel_listener->configure_bounds != NULL) {
		toplevel->xdg_toplevel_listener->configure_bounds(
				toplevel->data, xdg_toplevel, width, height);
	}
}

static void default_xdg_toplevel_handle_wm_capabilities(
		void *data, struct xdg_toplevel *xdg_toplevel, struct wl_array *capabilities) {
	struct rf_xdg_toplevel *toplevel = data;
	if (toplevel->xdg_toplevel_listener != NULL &&
			toplevel->xdg_toplevel_listener->wm_capabilities != NULL) {
		toplevel->xdg_toplevel_listener->wm_capabilities(
				toplevel->data, xdg_toplevel, capabilities);
	}
}

static const struct xdg_surface_listener toplevel_default_xdg_surface_listener = {
	.configure = toplevel_default_xdg_surface_handle_configure,
};

static const struct xdg_toplevel_listener default_xdg_toplevel_listener = {
	.configure = default_xdg_toplevel_handle_configure,
	.close = default_xdg_toplevel_handle_close,
	.configure_bounds = default_xdg_toplevel_handle_configure_bounds,
	.wm_capabilities = default_xdg_toplevel_handle_wm_capabilities,
};

void rf_xdg_toplevel_init(struct rf_xdg_toplevel *toplevel,
		const struct xdg_surface_listener *xdg_surface_listener,
		const struct xdg_toplevel_listener *xdg_toplevel_listener, void *data) {
	assert(rf_g_xdg_wm_base.global != NULL);

	memset(toplevel, 0, sizeof(*toplevel));
	toplevel->wl_surface = wl_compositor_create_surface(rf_g_wl_compositor.global);
	toplevel->xdg_surface =
			xdg_wm_base_get_xdg_surface(rf_g_xdg_wm_base.global, toplevel->wl_surface);
	xdg_surface_add_listener(
			toplevel->xdg_surface, &toplevel_default_xdg_surface_listener, toplevel);
	toplevel->xdg_toplevel = xdg_surface_get_toplevel(toplevel->xdg_surface);
	xdg_toplevel_add_listener(toplevel->xdg_toplevel, &default_xdg_toplevel_listener, toplevel);

	toplevel->xdg_surface_listener = xdg_surface_listener;
	toplevel->xdg_toplevel_listener = xdg_toplevel_listener;

	toplevel->data = data;
}

static void popup_default_xdg_surface_handle_configure(
		void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	struct rf_xdg_popup *popup = data;
	if (popup->xdg_surface_listener != NULL && popup->xdg_surface_listener->configure != NULL) {
		popup->xdg_surface_listener->configure(popup->data, xdg_surface, serial);
	}
}

static void default_xdg_popup_handle_configure(void *data, struct xdg_popup *xdg_popup, int32_t x,
		int32_t y, int32_t width, int32_t height) {
	struct rf_xdg_popup *popup = data;
	if (popup->xdg_popup_listener != NULL && popup->xdg_popup_listener->configure != NULL) {
		popup->xdg_popup_listener->configure(popup->data, xdg_popup, x, y, width, height);
	}
}

static void default_xdg_popup_handle_popup_done(void *data, struct xdg_popup *xdg_popup) {
	struct rf_xdg_popup *popup = data;
	if (popup->xdg_popup_listener != NULL && popup->xdg_popup_listener->popup_done != NULL) {
		popup->xdg_popup_listener->popup_done(popup->data, xdg_popup);
	}
}

static void default_xdg_popup_handle_repositioned(
		void *data, struct xdg_popup *xdg_popup, uint32_t token) {
	struct rf_xdg_popup *popup = data;
	if (popup->xdg_popup_listener != NULL && popup->xdg_popup_listener->repositioned != NULL) {
		popup->xdg_popup_listener->repositioned(popup->data, xdg_popup, token);
	}
}

static const struct xdg_surface_listener popup_default_xdg_surface_listener = {
	.configure = popup_default_xdg_surface_handle_configure,
};

static const struct xdg_popup_listener default_xdg_popup_listener = {
	.configure = default_xdg_popup_handle_configure,
	.popup_done = default_xdg_popup_handle_popup_done,
	.repositioned = default_xdg_popup_handle_repositioned,
};

void rf_xdg_popup_init(struct rf_xdg_popup *popup, struct xdg_surface *parent,
		struct xdg_positioner *positioner, const struct xdg_surface_listener *xdg_surface_listener,
		const struct xdg_popup_listener *xdg_popup_listener, void *data) {
	assert(rf_g_xdg_wm_base.global != NULL);

	if (positioner == NULL) {
		positioner = xdg_wm_base_create_positioner(rf_g_xdg_wm_base.global);
		xdg_positioner_set_size(positioner, 1, 1);
		xdg_positioner_set_anchor_rect(positioner, 0, 0, 1, 1);
	}

	memset(popup, 0, sizeof(*popup));
	popup->wl_surface = wl_compositor_create_surface(rf_g_wl_compositor.global);
	popup->xdg_surface = xdg_wm_base_get_xdg_surface(rf_g_xdg_wm_base.global, popup->wl_surface);
	xdg_surface_add_listener(popup->xdg_surface, &popup_default_xdg_surface_listener, popup);
	popup->xdg_popup = xdg_surface_get_popup(popup->xdg_surface, parent, positioner);
	xdg_popup_add_listener(popup->xdg_popup, &default_xdg_popup_listener, popup);

	popup->xdg_surface_listener = xdg_surface_listener;
	popup->xdg_popup_listener = xdg_popup_listener;

	popup->data = data;
}

static void xdg_wm_base_handle_ping(void *data, struct xdg_wm_base *xdg_wm_base, uint32_t serial) {
	xdg_wm_base_pong(xdg_wm_base, serial);
}

static const struct xdg_wm_base_listener xdg_wm_base_listener = {
	.ping = xdg_wm_base_handle_ping,
};

void rf_xdg_wm_base_add_ponger(void) {
	assert(rf_g_xdg_wm_base.global != NULL);
	xdg_wm_base_add_listener(rf_g_xdg_wm_base.global, &xdg_wm_base_listener, NULL);
}

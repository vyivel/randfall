#ifndef COMMON_H
#define COMMON_H

#include "randfall.h"

enum rf_expected_result {
	RF_EXPECTED_NONE,
	RF_EXPECTED_SUCCESS,
	RF_EXPECTED_ERROR,
};

struct rf_common_ctx {
	enum rf_expected_result expected_result;

	struct wl_list globals;
};

extern struct rf_common_ctx rf_common_ctx;

#endif

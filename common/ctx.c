#include <asm-generic/errno.h>
#include <stdlib.h>
#include <wayland-client-core.h>
#include <wayland-client-protocol.h>

#include "common.h"
#include "randfall.h"

struct rf_ctx rf_ctx = {0};

struct rf_common_ctx rf_common_ctx = {0};

static void atexit_handler(void) {
	int error = wl_display_get_error(rf_ctx.wl_display);
	if (error != 0 && error != EPROTO) {
		rf_util_abort("Non-protocol display error");
	}
	bool protocol_error = error == EPROTO;
	if (rf_common_ctx.expected_result == RF_EXPECTED_ERROR && !protocol_error) {
		rf_util_abort("Unexpected success");
	} else if (rf_common_ctx.expected_result == RF_EXPECTED_SUCCESS && protocol_error) {
		rf_util_abort("Unexpected error");
	}
}

void rf_ctx_start(void) {
	rf_ctx.wl_display = wl_display_connect(NULL);
	if (rf_ctx.wl_display == NULL) {
		rf_util_abort("Failed to connect");
	}
	rf_ctx.wl_registry = wl_display_get_registry(rf_ctx.wl_display);
	if (rf_ctx.wl_registry == NULL) {
		rf_util_abort("Failed to get wl_registry");
	}

	atexit(atexit_handler);

	wl_list_init(&rf_common_ctx.globals);
}

void rf_ctx_expect_success(void) {
	rf_common_ctx.expected_result = RF_EXPECTED_SUCCESS;
}

void rf_ctx_expect_error(void) {
	rf_common_ctx.expected_result = RF_EXPECTED_ERROR;
}

# randfall

A collection of Wayland clients to check how a compositor handles unusual
client behavior.

## Building

```sh
meson setup build/
ninja -C build/
```

## License

GPL-3.0-only

See `LICENSE` for more information.

Copyright © 2023 Kirill Primak

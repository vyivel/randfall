#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <wayland-client-core.h>
#include <wayland-client-protocol.h>

#include "randfall.h"
#include "xdg-shell-protocol.h"

struct toplevel {
	struct rf_xdg_toplevel base;
	bool mapped;
	bool closed;
};

static void xdg_surface_handle_configure(
		void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	struct toplevel *toplevel = data;
	if (toplevel->mapped) {
		return;
	}
	xdg_surface_ack_configure(xdg_surface, serial);
	struct rf_buffer *buffer = rf_buffer_create(100, 100);
	rf_buffer_fill(buffer, 0xFF, 0xFF, 0xFF);
	wl_surface_attach(toplevel->base.wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_commit(toplevel->base.wl_surface);
	toplevel->mapped = true;
}

static const struct xdg_surface_listener xdg_surface_listener = {
	.configure = xdg_surface_handle_configure,
};

static void xdg_toplevel_handle_close(void *data, struct xdg_toplevel *xdg_toplevel) {
	struct toplevel *toplevel = data;
	toplevel->closed = true;
}

static const struct xdg_toplevel_listener xdg_toplevel_listener = {
	.close = xdg_toplevel_handle_close,
};

int main(void) {
	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_shm.base, 1);
	rf_global_ensure(&rf_g_wl_compositor.base, 1);
	rf_global_ensure(&rf_g_wl_subcompositor.base, 1);
	rf_global_ensure(&rf_g_xdg_wm_base.base, 1);

	rf_global_collect_all();

	rf_xdg_wm_base_add_ponger();

	struct toplevel toplevel = {0};
	rf_xdg_toplevel_init(&toplevel.base, &xdg_surface_listener, &xdg_toplevel_listener, &toplevel);
	wl_surface_commit(toplevel.base.wl_surface);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (toplevel.mapped) {
			break;
		}
	}

	struct wl_surface *child = wl_compositor_create_surface(rf_g_wl_compositor.global);
	struct wl_subsurface *child_sub = wl_subcompositor_get_subsurface(
			rf_g_wl_subcompositor.global, child, toplevel.base.wl_surface);
	wl_subsurface_set_position(child_sub, 50, 50);
	wl_subsurface_set_desync(child_sub);
	wl_surface_commit(toplevel.base.wl_surface);

	struct rf_buffer *buffer;

	buffer = rf_buffer_create(100, 100);
	rf_buffer_fill(buffer, 0xFF, 0x00, 0x00);
	wl_surface_attach(child, buffer->wl_buffer, 0, 0);
	wl_surface_damage(child, 0, 0, 100, 100);

	wl_surface_commit(child);

	wl_display_roundtrip(rf_ctx.wl_display);
	sleep(1);

	buffer = rf_buffer_create(100, 100);
	rf_buffer_fill(buffer, 0x00, 0xFF, 0x00);
	wl_surface_attach(child, buffer->wl_buffer, 0, 0);
	wl_surface_damage(child, 0, 0, 100, 100);

	// The buffer itself is cleaned up later
	wl_buffer_destroy(buffer->wl_buffer);

	wl_surface_commit(child);

	wl_display_roundtrip(rf_ctx.wl_display);
	sleep(1);

	// And now is later
	munmap(buffer->data, buffer->size);
	free(buffer);

	buffer = rf_buffer_create(100, 100);
	rf_buffer_fill(buffer, 0x00, 0x00, 0xFF);
	wl_surface_attach(child, buffer->wl_buffer, 0, 0);
	wl_surface_damage(child, 0, 0, 100, 100);

	wl_surface_commit(child);
	// XXX: Weston (incorrectly) doesn't remap the subsurface without this
	wl_surface_commit(toplevel.base.wl_surface);

	wl_display_roundtrip(rf_ctx.wl_display);
	sleep(1);
}

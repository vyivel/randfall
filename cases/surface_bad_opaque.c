#include <unistd.h>
#include <wayland-client-protocol.h>

#include "randfall.h"
#include "xdg-shell-protocol.h"

struct toplevel {
	struct rf_xdg_toplevel base;
	bool mapped;
	bool closed;
};

static void xdg_surface_handle_configure(
		void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	struct toplevel *toplevel = data;
	xdg_surface_ack_configure(xdg_surface, serial);

	if (toplevel->mapped) {
		wl_surface_commit(toplevel->base.wl_surface);
		return;
	}

	struct rf_buffer *buffer = rf_buffer_create(100, 100);

	char *buf_data = buffer->data;
	for (int32_t i = 0; i < buffer->size; i += 4) {
		buf_data[i + 0] = 0x7F; // blue
		buf_data[i + 1] = 0x00; // green
		buf_data[i + 2] = 0x00; // red
		buf_data[i + 3] = 0x7F; // alpha
	}

	wl_surface_attach(toplevel->base.wl_surface, buffer->wl_buffer, 0, 0);

	struct wl_region *region = wl_compositor_create_region(rf_g_wl_compositor.global);
	wl_region_add(region, 0, 0, INT32_MAX, INT32_MAX);
	wl_surface_set_opaque_region(toplevel->base.wl_surface, region);
	wl_region_destroy(region);

	wl_surface_commit(toplevel->base.wl_surface);
	toplevel->mapped = true;
}

static const struct xdg_surface_listener xdg_surface_listener = {
	.configure = xdg_surface_handle_configure,
};

static void xdg_toplevel_handle_close(void *data, struct xdg_toplevel *xdg_toplevel) {
	struct toplevel *toplevel = data;
	toplevel->closed = true;
}

static const struct xdg_toplevel_listener xdg_toplevel_listener = {
	.close = xdg_toplevel_handle_close,
};

int main(void) {
	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_shm.base, 1);
	rf_global_ensure(&rf_g_wl_compositor.base, 5);
	rf_global_ensure(&rf_g_xdg_wm_base.base, 1);

	rf_global_collect_all();

	rf_xdg_wm_base_add_ponger();

	struct toplevel toplevel = {0};
	rf_xdg_toplevel_init(&toplevel.base, &xdg_surface_listener, &xdg_toplevel_listener, &toplevel);
	wl_surface_commit(toplevel.base.wl_surface);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (toplevel.closed) {
			break;
		}
	}
}

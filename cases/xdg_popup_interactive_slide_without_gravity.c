#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wayland-client-protocol.h>

#include "randfall.h"
#include "xdg-shell-protocol.h"

struct toplevel {
	struct rf_xdg_toplevel base;
	bool mapped;
	bool closed;
};

struct popup {
	struct rf_xdg_popup base;
	bool created;
	bool mapped;
};

struct pointer {
	struct rf_wl_pointer base;
	bool pressed;
	uint32_t serial;
};

static struct toplevel toplevel = {0};
static struct popup popup = {0};

static const int toplevel_width = 100, toplevel_height = 100;
static const int popup_width = 3000, popup_height = 200;

static void wl_seat_handle_capabilities(
		void *data, struct wl_seat *wl_seat, uint32_t capabilities) {
	uint32_t *out = data;
	*out = capabilities;
}

static const struct wl_seat_listener wl_seat_listener = {
	.capabilities = wl_seat_handle_capabilities,
};

static void toplevel_xdg_surface_handle_configure(
		void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	struct toplevel *toplevel = data;
	if (toplevel->mapped) {
		return;
	}
	xdg_surface_ack_configure(xdg_surface, serial);
	struct rf_buffer *buffer = rf_buffer_create(toplevel_width, toplevel_height);
	rf_buffer_fill(buffer, 0xFF, 0xFF, 0xFF);
	wl_surface_attach(toplevel->base.wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_commit(toplevel->base.wl_surface);
	toplevel->mapped = true;
}

static const struct xdg_surface_listener toplevel_xdg_surface_listener = {
	.configure = toplevel_xdg_surface_handle_configure,
};

static void toplevel_xdg_toplevel_handle_close(void *data, struct xdg_toplevel *xdg_toplevel) {
	struct toplevel *toplevel = data;
	toplevel->closed = true;
}

static const struct xdg_toplevel_listener toplevel_xdg_toplevel_listener = {
	.close = toplevel_xdg_toplevel_handle_close,
};

static void popup_xdg_surface_handle_configure(
		void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	struct popup *popup = data;
	if (popup->mapped) {
		return;
	}
	xdg_surface_ack_configure(xdg_surface, serial);
	struct rf_buffer *buffer = rf_buffer_create(popup_width, popup_height);

	char *buf_data = buffer->data;
	for (int32_t x = 0; x < popup_width; x++) {
		uint8_t red, green, blue;
		if (x < 4 || x >= popup_width - 4) {
			red = blue = 0;
			green = 0xFF;
		} else {
			red = blue = powf((float)x / popup_width, 2.2) * 0xFF + 0.5;
			green = 0;
		}
		for (int32_t y = 0; y < popup_height; y++) {
			int32_t i = (y * popup_width + x) * 4;
			buf_data[i + 0] = blue;
			buf_data[i + 1] = green;
			buf_data[i + 2] = red;
			buf_data[i + 3] = 0xFF;
		}
	}

	wl_surface_attach(popup->base.wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_commit(popup->base.wl_surface);
	popup->mapped = true;
}

static const struct xdg_surface_listener popup_xdg_surface_listener = {
	.configure = popup_xdg_surface_handle_configure,
};

static void wl_pointer_handle_button(void *data, struct wl_pointer *wl_pointer, uint32_t serial,
		uint32_t time, uint32_t button, uint32_t state) {
	if (state != WL_POINTER_BUTTON_STATE_PRESSED) {
		return;
	}
	if (popup.created) {
		return;
	}

	struct xdg_positioner *positioner = xdg_wm_base_create_positioner(rf_g_xdg_wm_base.global);
	xdg_positioner_set_size(positioner, popup_width, popup_height);
	xdg_positioner_set_anchor_rect(positioner, 0, 0, toplevel_width, toplevel_height);
	xdg_positioner_set_constraint_adjustment(
			positioner, XDG_POSITIONER_CONSTRAINT_ADJUSTMENT_SLIDE_X);

	rf_xdg_popup_init(&popup.base, toplevel.base.xdg_surface, positioner,
			&popup_xdg_surface_listener, NULL, &popup);

	wl_surface_commit(popup.base.wl_surface);

	popup.created = true;
}

static const struct wl_pointer_listener wl_pointer_listener = {
	.button = wl_pointer_handle_button,
};

int main(void) {
	RF_GLOBAL_DEFINE(wl_seat, g_seat);

	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_shm.base, 1);
	rf_global_ensure(&rf_g_wl_compositor.base, 1);
	rf_global_ensure(&rf_g_xdg_wm_base.base, 1);

	rf_global_ensure(&g_seat.base, 1);

	rf_global_collect_all();

	rf_xdg_wm_base_add_ponger();

	uint32_t capabilities = 0;
	wl_seat_add_listener(g_seat.global, &wl_seat_listener, &capabilities);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if ((capabilities & WL_SEAT_CAPABILITY_POINTER) != 0) {
			break;
		}
	}

	struct pointer pointer = {0};
	rf_wl_pointer_init(&pointer.base, g_seat.global, &wl_pointer_listener, &pointer);

	rf_xdg_toplevel_init(&toplevel.base, &toplevel_xdg_surface_listener,
			&toplevel_xdg_toplevel_listener, &toplevel);
	wl_surface_commit(toplevel.base.wl_surface);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (toplevel.closed) {
			break;
		}
	}
}

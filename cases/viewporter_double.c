#include <wayland-client-protocol.h>

#include "randfall.h"
#include "viewporter-protocol.h"

int main(void) {
	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_compositor.base, 1);
	rf_global_ensure(&rf_g_wp_viewporter.base, 1);

	rf_global_collect_all();

	struct wl_surface *surface = wl_compositor_create_surface(rf_g_wl_compositor.global);
	wp_viewporter_get_viewport(rf_g_wp_viewporter.global, surface);
	wp_viewporter_get_viewport(rf_g_wp_viewporter.global, surface);

	rf_ctx_expect_error();

	wl_display_roundtrip(rf_ctx.wl_display);
}

#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <wayland-client-core.h>
#include <wayland-client-protocol.h>

#include "randfall.h"
#include "wlr-layer-shell-unstable-v1-protocol.h"

struct layer_surface {
	struct rf_wlr_layer_surface base;
	bool acked;
	uint32_t width, height;
};

static void wlr_layer_surface_handle_configure(void *data,
		struct zwlr_layer_surface_v1 *zwlr_layer_surface_v1, uint32_t serial, uint32_t width,
		uint32_t height) {
	struct layer_surface *layer_surface = data;
	zwlr_layer_surface_v1_ack_configure(zwlr_layer_surface_v1, serial);
	layer_surface->acked = true;
	layer_surface->width = width;
	layer_surface->height = height;
}

static const struct zwlr_layer_surface_v1_listener zwlr_layer_surface_v1_listener = {
	.configure = wlr_layer_surface_handle_configure,
};

int main(void) {
	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_shm.base, 1);
	rf_global_ensure(&rf_g_wl_compositor.base, 1);
	rf_global_ensure(&rf_g_zwlr_layer_shell_v1.base, 1);

	rf_global_collect_all();

	struct layer_surface layer_surface = {0};
	rf_wlr_layer_surface_init(&layer_surface.base, NULL, ZWLR_LAYER_SHELL_V1_LAYER_TOP, "randfall",
			&zwlr_layer_surface_v1_listener, &layer_surface);

	zwlr_layer_surface_v1_set_size(layer_surface.base.zwlr_layer_surface_v1, 0, 32);
	zwlr_layer_surface_v1_set_exclusive_zone(layer_surface.base.zwlr_layer_surface_v1, 32);
	zwlr_layer_surface_v1_set_anchor(layer_surface.base.zwlr_layer_surface_v1,
			ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT | ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT |
					ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM);

	wl_surface_commit(layer_surface.base.wl_surface);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (layer_surface.acked) {
			break;
		}
	}

	wl_display_roundtrip(rf_ctx.wl_display);
	sleep(1);

	struct rf_buffer *buffer = rf_buffer_create(layer_surface.width, layer_surface.height);
	rf_buffer_fill(buffer, 0xFF, 0xFF, 0xFF);
	wl_surface_attach(layer_surface.base.wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_commit(layer_surface.base.wl_surface);

	wl_display_roundtrip(rf_ctx.wl_display);
	sleep(1);

	wl_surface_attach(layer_surface.base.wl_surface, NULL, 0, 0);
	wl_surface_commit(layer_surface.base.wl_surface);

	wl_display_roundtrip(rf_ctx.wl_display);
	sleep(1);
}

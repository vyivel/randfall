#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <wayland-client-core.h>
#include <wayland-client-protocol.h>

#include "randfall.h"
#include "xdg-shell-protocol.h"

struct toplevel {
	struct rf_xdg_toplevel base;
	int width, height;
	bool mapped;
};

static struct toplevel toplevel = {
	.width = 100,
	.height = 100,
};

static RF_GLOBAL_DEFINE(wl_seat, g_seat);

static void wl_seat_handle_capabilities(
		void *data, struct wl_seat *wl_seat, uint32_t capabilities) {
	uint32_t *out = data;
	*out = capabilities;
}

static const struct wl_seat_listener wl_seat_listener = {
	.capabilities = wl_seat_handle_capabilities,
};

static void wl_pointer_handle_button(void *data, struct wl_pointer *wl_pointer, uint32_t serial,
		uint32_t time, uint32_t button, uint32_t state) {
	if (state == WL_POINTER_BUTTON_STATE_PRESSED) {
		xdg_toplevel_resize(
				toplevel.base.xdg_toplevel, g_seat.global, serial, XDG_TOPLEVEL_RESIZE_EDGE_NONE);
	}
}

static const struct wl_pointer_listener wl_pointer_listener = {
	.button = wl_pointer_handle_button,
};

static void toplevel_xdg_toplevel_handle_configure(void *data, struct xdg_toplevel *xdg_toplevel,
		int32_t width, int32_t height, struct wl_array *state) {
	struct toplevel *toplevel = data;
	if (width != 0) {
		toplevel->width = width;
	}
	if (height != 0) {
		toplevel->height = height;
	}
}

static const struct xdg_toplevel_listener toplevel_xdg_toplevel_listener = {
	.configure = toplevel_xdg_toplevel_handle_configure,
};

static void toplevel_xdg_surface_handle_configure(
		void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	struct toplevel *toplevel = data;
	xdg_surface_ack_configure(xdg_surface, serial);
	struct rf_buffer *buffer = rf_buffer_create(toplevel->width, toplevel->height);
	rf_buffer_fill(buffer, 0xFF, 0xFF, 0xFF);
	wl_surface_attach(toplevel->base.wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_commit(toplevel->base.wl_surface);
	toplevel->mapped = true;
}

static const struct xdg_surface_listener toplevel_xdg_surface_listener = {
	.configure = toplevel_xdg_surface_handle_configure,
};

int main(void) {
	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_shm.base, 1);
	rf_global_ensure(&rf_g_wl_compositor.base, 1);
	rf_global_ensure(&rf_g_xdg_wm_base.base, 1);

	rf_global_ensure(&g_seat.base, 1);

	rf_global_collect_all();

	rf_xdg_wm_base_add_ponger();

	uint32_t capabilities = 0;
	wl_seat_add_listener(g_seat.global, &wl_seat_listener, &capabilities);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if ((capabilities & WL_SEAT_CAPABILITY_POINTER) != 0) {
			break;
		}
	}

	struct rf_wl_pointer pointer = {0};
	rf_wl_pointer_init(&pointer, g_seat.global, &wl_pointer_listener, NULL);

	rf_xdg_toplevel_init(&toplevel.base, &toplevel_xdg_surface_listener,
			&toplevel_xdg_toplevel_listener, &toplevel);
	wl_surface_commit(toplevel.base.wl_surface);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (toplevel.mapped) {
			break;
		}
	}

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		// Do nothing
	}
}

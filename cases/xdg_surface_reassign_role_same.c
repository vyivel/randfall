#include <wayland-client-protocol.h>

#include "randfall.h"
#include "xdg-shell-protocol.h"

int main(void) {
	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_compositor.base, 1);
	rf_global_ensure(&rf_g_xdg_wm_base.base, 1);

	rf_global_collect_all();

	struct wl_surface *surface = wl_compositor_create_surface(rf_g_wl_compositor.global);
	struct xdg_surface *xdg_surface = xdg_wm_base_get_xdg_surface(rf_g_xdg_wm_base.global, surface);
	xdg_toplevel_destroy(xdg_surface_get_toplevel(xdg_surface));
	xdg_surface_get_toplevel(xdg_surface);

	rf_ctx_expect_success();

	wl_display_roundtrip(rf_ctx.wl_display);
}

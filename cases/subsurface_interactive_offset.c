#include <unistd.h>
#include <wayland-client-protocol.h>

#include "randfall.h"
#include "xdg-shell-protocol.h"

struct toplevel {
	struct rf_xdg_toplevel base;
	bool mapped;
	bool closed;
};

static void xdg_surface_handle_configure(
		void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	struct toplevel *toplevel = data;
	if (toplevel->mapped) {
		return;
	}
	xdg_surface_ack_configure(xdg_surface, serial);
	struct rf_buffer *buffer = rf_buffer_create(100, 100);
	rf_buffer_fill(buffer, 0xFF, 0x00, 0x00);
	wl_surface_attach(toplevel->base.wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_commit(toplevel->base.wl_surface);
	toplevel->mapped = true;
}

static const struct xdg_surface_listener xdg_surface_listener = {
	.configure = xdg_surface_handle_configure,
};

static void xdg_toplevel_handle_close(void *data, struct xdg_toplevel *xdg_toplevel) {
	struct toplevel *toplevel = data;
	toplevel->closed = true;
}

static const struct xdg_toplevel_listener xdg_toplevel_listener = {
	.close = xdg_toplevel_handle_close,
};

int main(void) {
	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_shm.base, 1);
	rf_global_ensure(&rf_g_wl_compositor.base, 5);
	rf_global_ensure(&rf_g_wl_subcompositor.base, 1);
	rf_global_ensure(&rf_g_xdg_wm_base.base, 1);

	rf_global_collect_all();

	rf_xdg_wm_base_add_ponger();

	struct toplevel toplevel = {0};
	rf_xdg_toplevel_init(&toplevel.base, &xdg_surface_listener, &xdg_toplevel_listener, &toplevel);
	wl_surface_commit(toplevel.base.wl_surface);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (toplevel.mapped) {
			break;
		}
	}

	struct wl_surface *surface = wl_compositor_create_surface(rf_g_wl_compositor.global);
	struct wl_subsurface *subsurface = wl_subcompositor_get_subsurface(
			rf_g_wl_subcompositor.global, surface, toplevel.base.wl_surface);

	struct rf_buffer *buffer = rf_buffer_create(100, 100);
	rf_buffer_fill(buffer, 0x00, 0xFF, 0x00);
	wl_surface_attach(surface, buffer->wl_buffer, 0, 0);
	wl_surface_commit(surface);

	wl_subsurface_set_position(subsurface, 50, 50);
	wl_surface_commit(toplevel.base.wl_surface);

	wl_surface_offset(surface, -100, -100);
	wl_surface_commit(surface);
	wl_surface_commit(toplevel.base.wl_surface);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (toplevel.closed) {
			break;
		}
	}
}

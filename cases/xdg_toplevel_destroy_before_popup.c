#include <wayland-client-protocol.h>

#include "randfall.h"
#include "xdg-shell-protocol.h"

int main(void) {
	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_compositor.base, 1);
	rf_global_ensure(&rf_g_xdg_wm_base.base, 1);

	rf_global_collect_all();

	struct rf_xdg_toplevel toplevel;
	rf_xdg_toplevel_init(&toplevel, NULL, NULL, NULL);

	struct rf_xdg_popup popup;
	rf_xdg_popup_init(&popup, toplevel.xdg_surface, NULL, NULL, NULL, NULL);

	xdg_toplevel_destroy(toplevel.xdg_toplevel);

	wl_display_roundtrip(rf_ctx.wl_display);
}

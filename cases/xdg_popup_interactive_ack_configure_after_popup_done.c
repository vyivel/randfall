#include <stdio.h>
#include <string.h>
#include <wayland-client-protocol.h>

#include "randfall.h"
#include "xdg-shell-protocol.h"

struct toplevel {
	struct rf_xdg_toplevel base;
	bool mapped;
};

struct popup {
	struct rf_xdg_popup base;
	bool mapped;
	bool done;
	bool repositioned;
	bool has_reposition_serial;
	uint32_t reposition_serial;
};

struct pointer {
	struct rf_wl_pointer base;
	bool pressed;
	uint32_t serial;
};

static void wl_seat_handle_capabilities(
		void *data, struct wl_seat *wl_seat, uint32_t capabilities) {
	uint32_t *out = data;
	*out = capabilities;
}

static const struct wl_seat_listener wl_seat_listener = {
	.capabilities = wl_seat_handle_capabilities,
};

static void wl_pointer_handle_button(void *data, struct wl_pointer *wl_pointer, uint32_t serial,
		uint32_t time, uint32_t button, uint32_t state) {
	struct pointer *pointer = data;
	if (state == WL_POINTER_BUTTON_STATE_PRESSED) {
		pointer->pressed = true;
		pointer->serial = serial;
	}
}

static const struct wl_pointer_listener wl_pointer_listener = {
	.button = wl_pointer_handle_button,
};

static void toplevel_xdg_surface_handle_configure(
		void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	struct toplevel *toplevel = data;
	if (toplevel->mapped) {
		return;
	}
	xdg_surface_ack_configure(xdg_surface, serial);
	struct rf_buffer *buffer = rf_buffer_create(100, 100);
	rf_buffer_fill(buffer, 0xFF, 0xFF, 0xFF);
	wl_surface_attach(toplevel->base.wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_commit(toplevel->base.wl_surface);
	toplevel->mapped = true;
}

static const struct xdg_surface_listener toplevel_xdg_surface_listener = {
	.configure = toplevel_xdg_surface_handle_configure,
};

static void popup_xdg_surface_handle_configure(
		void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	struct popup *popup = data;
	if (popup->repositioned && !popup->has_reposition_serial) {
		popup->has_reposition_serial = true;
		popup->reposition_serial = serial;
	}
	if (popup->mapped) {
		return;
	}
	xdg_surface_ack_configure(xdg_surface, serial);
	struct rf_buffer *buffer = rf_buffer_create(100, 100);
	rf_buffer_fill(buffer, 0x00, 0x00, 0xFF);
	wl_surface_attach(popup->base.wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_commit(popup->base.wl_surface);
	popup->mapped = true;
}

static const struct xdg_surface_listener popup_xdg_surface_listener = {
	.configure = popup_xdg_surface_handle_configure,
};

static void popup_xdg_popup_handle_popup_done(void *data, struct xdg_popup *xdg_popup) {
	struct popup *popup = data;
	popup->done = true;
}

static void popup_xdg_popup_handle_repositioned(
		void *data, struct xdg_popup *xdg_popup, uint32_t token) {
	struct popup *popup = data;
	popup->repositioned = true;
}

static const struct xdg_popup_listener popup_xdg_popup_listener = {
	.popup_done = popup_xdg_popup_handle_popup_done,
	.repositioned = popup_xdg_popup_handle_repositioned,
};

int main(void) {
	RF_GLOBAL_DEFINE(wl_seat, g_seat);

	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_shm.base, 1);
	rf_global_ensure(&rf_g_wl_compositor.base, 1);
	rf_global_ensure(&rf_g_xdg_wm_base.base, 3);

	rf_global_ensure(&g_seat.base, 1);

	rf_global_collect_all();

	rf_xdg_wm_base_add_ponger();

	uint32_t capabilities = 0;
	wl_seat_add_listener(g_seat.global, &wl_seat_listener, &capabilities);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if ((capabilities & WL_SEAT_CAPABILITY_POINTER) != 0) {
			break;
		}
	}

	struct pointer pointer = {0};
	rf_wl_pointer_init(&pointer.base, g_seat.global, &wl_pointer_listener, &pointer);

	struct toplevel toplevel = {0};
	rf_xdg_toplevel_init(&toplevel.base, &toplevel_xdg_surface_listener, NULL, &toplevel);
	wl_surface_commit(toplevel.base.wl_surface);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (toplevel.mapped) {
			break;
		}
	}

	printf("Waiting for wl_pointer.button\n");

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (pointer.pressed) {
			break;
		}
	}

	struct popup popup = {0};
	rf_xdg_popup_init(&popup.base, toplevel.base.xdg_surface, NULL, &popup_xdg_surface_listener,
			&popup_xdg_popup_listener, &popup);

	xdg_popup_grab(popup.base.xdg_popup, g_seat.global, pointer.serial);
	wl_surface_commit(popup.base.wl_surface);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (popup.mapped) {
			break;
		}
	}

	struct xdg_positioner *positioner = xdg_wm_base_create_positioner(rf_g_xdg_wm_base.global);
	xdg_positioner_set_size(positioner, 1, 1);
	xdg_positioner_set_anchor_rect(positioner, 0, 0, 1, 1);
	xdg_positioner_set_offset(positioner, 80, 80);
	xdg_popup_reposition(popup.base.xdg_popup, positioner, 42);

	printf("Waiting for xdg_surface.configure\n");

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (popup.has_reposition_serial) {
			break;
		}
	}

	printf("Waiting for xdg_popup.popup_done\n");

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (popup.done) {
			break;
		}
	}

	xdg_surface_ack_configure(popup.base.xdg_surface, popup.reposition_serial);

	wl_display_roundtrip(rf_ctx.wl_display);
}

#include <wayland-client-protocol.h>

#include "randfall.h"

int main(void) {
	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_compositor.base, 1);
	rf_global_ensure(&rf_g_wl_subcompositor.base, 1);

	rf_global_collect_all();

	struct wl_surface *foo = wl_compositor_create_surface(rf_g_wl_compositor.global);
	struct wl_surface *bar = wl_compositor_create_surface(rf_g_wl_compositor.global);
	wl_subcompositor_get_subsurface(rf_g_wl_subcompositor.global, foo, bar);
	wl_subcompositor_get_subsurface(rf_g_wl_subcompositor.global, foo, bar);

	rf_ctx_expect_error();

	wl_display_roundtrip(rf_ctx.wl_display);
}

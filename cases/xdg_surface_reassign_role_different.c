#include <wayland-client-protocol.h>

#include "randfall.h"
#include "xdg-shell-protocol.h"

int main(void) {
	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_compositor.base, 1);
	rf_global_ensure(&rf_g_xdg_wm_base.base, 1);

	rf_global_collect_all();

	struct wl_surface *surface = wl_compositor_create_surface(rf_g_wl_compositor.global);
	struct xdg_surface *xdg_surface = xdg_wm_base_get_xdg_surface(rf_g_xdg_wm_base.global, surface);
	xdg_toplevel_destroy(xdg_surface_get_toplevel(xdg_surface));

	struct xdg_positioner *xdg_positioner = xdg_wm_base_create_positioner(rf_g_xdg_wm_base.global);
	xdg_positioner_set_size(xdg_positioner, 1, 1);
	xdg_positioner_set_anchor_rect(xdg_positioner, 0, 0, 1, 1);
	xdg_surface_get_popup(xdg_surface, NULL, xdg_positioner);

	rf_ctx_expect_error();

	wl_display_roundtrip(rf_ctx.wl_display);
}

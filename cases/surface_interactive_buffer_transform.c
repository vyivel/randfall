#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <wayland-client-core.h>
#include <wayland-client-protocol.h>

#include "randfall.h"
#include "xdg-shell-protocol.h"

struct toplevel {
	struct rf_xdg_toplevel base;
	struct wp_viewport *viewport;
	bool mapped;
	bool closed;
};

struct buffer_args {
	int w, h;
	double sx, sy, sw, sh;
	int dw, dh;
	int s;
	enum wl_output_transform t;
};

static struct buffer_args buf_args = {
	.w = 600,
	.h = 400,
	.sx = -1.0,
	.sy = -1.0,
	.sw = -1.0,
	.sh = -1.0,
	.dw = -1,
	.dh = -1,
	.s = 1,
	.t = WL_OUTPUT_TRANSFORM_NORMAL,
};

static void xdg_surface_handle_configure(
		void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	struct toplevel *toplevel = data;
	xdg_surface_ack_configure(xdg_surface, serial);

	struct wl_surface *wl_surface = toplevel->base.wl_surface;

	if (toplevel->mapped) {
		wl_surface_commit(wl_surface);
		return;
	}

	struct rf_buffer *buffer = rf_buffer_create(buf_args.w, buf_args.h);
	const char *src_data = rf_image_data.data; // RGB
	char *dst_data = buffer->data; // BGRA
	for (int y = 0; y < buf_args.h; y++) {
		int src_y = y % rf_image_data.height;
		for (int x = 0; x < buf_args.w; x++) {
			int src_x = x % rf_image_data.width;
			int src_i = (src_y * rf_image_data.width + src_x) * 3;
			int i = (y * buf_args.w + x) * 4;
			dst_data[i + 0] = src_data[src_i + 2];
			dst_data[i + 1] = src_data[src_i + 1];
			dst_data[i + 2] = src_data[src_i + 0];
			dst_data[i + 3] = 0xFF;
		}
	}

	wp_viewport_set_source(toplevel->viewport, wl_fixed_from_double(buf_args.sx),
			wl_fixed_from_double(buf_args.sy), wl_fixed_from_double(buf_args.sw),
			wl_fixed_from_double(buf_args.sh));
	wp_viewport_set_destination(toplevel->viewport, buf_args.dw, buf_args.dh);

	wl_surface_set_buffer_scale(wl_surface, buf_args.s);
	wl_surface_set_buffer_transform(wl_surface, buf_args.t);

	wl_surface_attach(wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_commit(wl_surface);

	toplevel->mapped = true;
}

static const struct xdg_surface_listener xdg_surface_listener = {
	.configure = xdg_surface_handle_configure,
};

static void xdg_toplevel_handle_close(void *data, struct xdg_toplevel *xdg_toplevel) {
	struct toplevel *toplevel = data;
	toplevel->closed = true;
}

static const struct xdg_toplevel_listener xdg_toplevel_listener = {
	.close = xdg_toplevel_handle_close,
};

static void die_help(const char *prog) {
	fprintf(stderr,
			"%s "
			"[w <width>] [h <height>] "
			"[sx <src x>] [sy <src y>] "
			"[sw <src width>] [sh <src height>] "
			"[dw <dst width>] [dh <dst height>] "
			"[s <scale>] "
			"[t <transform>] "
			"\n"
			"Transforms: 0|90|180|270|f-0|f-90|f-180|f-270\n",
			prog);
	exit(1);
}

int main(int argc, char **argv) {
	const char *prog = argv[0];
	--argc;
	++argv;

	if (argc % 2 != 0) {
		die_help(prog);
	}
	for (int i = 0; i < argc; i += 2) {
		const char *arg = argv[i];
		const char *val = argv[i + 1];
		if (strcmp(arg, "w") == 0) {
			buf_args.w = atoi(val);
		} else if (strcmp(arg, "h") == 0) {
			buf_args.h = atoi(val);
		} else if (strcmp(arg, "sx") == 0) {
			buf_args.sx = atof(val);
		} else if (strcmp(arg, "sy") == 0) {
			buf_args.sy = atof(val);
		} else if (strcmp(arg, "sw") == 0) {
			buf_args.sw = atof(val);
		} else if (strcmp(arg, "sh") == 0) {
			buf_args.sh = atof(val);
		} else if (strcmp(arg, "dw") == 0) {
			buf_args.dw = atoi(val);
		} else if (strcmp(arg, "dh") == 0) {
			buf_args.dh = atoi(val);
		} else if (strcmp(arg, "s") == 0) {
			buf_args.s = atoi(val);
		} else if (strcmp(arg, "t") == 0) {
			if (strcmp(val, "0") == 0) {
				buf_args.t = WL_OUTPUT_TRANSFORM_NORMAL;
			} else if (strcmp(val, "90") == 0) {
				buf_args.t = WL_OUTPUT_TRANSFORM_90;
			} else if (strcmp(val, "180") == 0) {
				buf_args.t = WL_OUTPUT_TRANSFORM_180;
			} else if (strcmp(val, "270") == 0) {
				buf_args.t = WL_OUTPUT_TRANSFORM_270;
			} else if (strcmp(val, "f-0") == 0) {
				buf_args.t = WL_OUTPUT_TRANSFORM_FLIPPED;
			} else if (strcmp(val, "f-90") == 0) {
				buf_args.t = WL_OUTPUT_TRANSFORM_FLIPPED_90;
			} else if (strcmp(val, "f-180") == 0) {
				buf_args.t = WL_OUTPUT_TRANSFORM_FLIPPED_180;
			} else if (strcmp(val, "f-270") == 0) {
				buf_args.t = WL_OUTPUT_TRANSFORM_FLIPPED_270;
			} else {
				die_help(prog);
			}
		} else {
			die_help(prog);
		}
	}

	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_shm.base, 1);
	rf_global_ensure(&rf_g_wl_compositor.base, 3);
	rf_global_ensure(&rf_g_wp_viewporter.base, 1);
	rf_global_ensure(&rf_g_xdg_wm_base.base, 1);

	rf_global_collect_all();

	rf_xdg_wm_base_add_ponger();

	struct toplevel toplevel = {0};
	rf_xdg_toplevel_init(&toplevel.base, &xdg_surface_listener, &xdg_toplevel_listener, &toplevel);
	toplevel.viewport =
			wp_viewporter_get_viewport(rf_g_wp_viewporter.global, toplevel.base.wl_surface);
	wl_surface_commit(toplevel.base.wl_surface);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (toplevel.closed) {
			break;
		}
	}
}

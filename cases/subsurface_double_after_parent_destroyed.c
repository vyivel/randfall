#include <wayland-client-protocol.h>

#include "randfall.h"

int main(void) {
	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_compositor.base, 1);
	rf_global_ensure(&rf_g_wl_subcompositor.base, 1);

	rf_global_collect_all();

	struct wl_surface *parent1 = wl_compositor_create_surface(rf_g_wl_compositor.global);
	struct wl_surface *parent2 = wl_compositor_create_surface(rf_g_wl_compositor.global);
	struct wl_surface *subsurface = wl_compositor_create_surface(rf_g_wl_compositor.global);
	wl_subcompositor_get_subsurface(rf_g_wl_subcompositor.global, subsurface, parent1);
	wl_surface_destroy(parent1);

	// Optional
	wl_surface_commit(subsurface);

	wl_subcompositor_get_subsurface(rf_g_wl_subcompositor.global, subsurface, parent2);

	rf_ctx_expect_error();

	wl_display_roundtrip(rf_ctx.wl_display);
}

#include <wayland-client-protocol.h>

#include "randfall.h"
#include "xdg-shell-protocol.h"

int main(void) {
	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_compositor.base, 1);
	rf_global_ensure(&rf_g_xdg_wm_base.base, 1);

	rf_global_collect_all();

	struct wl_surface *parent_surface = wl_compositor_create_surface(rf_g_wl_compositor.global);
	struct xdg_surface *parent_xdg_surface =
			xdg_wm_base_get_xdg_surface(rf_g_xdg_wm_base.global, parent_surface);

	struct rf_xdg_popup popup;
	rf_xdg_popup_init(&popup, parent_xdg_surface, NULL, NULL, NULL, NULL);

	xdg_surface_destroy(parent_xdg_surface);

	wl_surface_commit(popup.wl_surface);

	wl_display_roundtrip(rf_ctx.wl_display);
}

#include <wayland-client-protocol.h>

#include "randfall.h"
#include "xdg-shell-protocol.h"

int main(void) {
	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_compositor.base, 1);
	rf_global_ensure(&rf_g_xdg_wm_base.base, 1);

	rf_global_collect_all();

	struct wl_surface *foo_surface = wl_compositor_create_surface(rf_g_wl_compositor.global);
	struct xdg_surface *foo_xdg_surface =
			xdg_wm_base_get_xdg_surface(rf_g_xdg_wm_base.global, foo_surface);

	struct wl_surface *bar_surface = wl_compositor_create_surface(rf_g_wl_compositor.global);
	struct xdg_surface *bar_xdg_surface =
			xdg_wm_base_get_xdg_surface(rf_g_xdg_wm_base.global, bar_surface);

	struct xdg_positioner *positioner = xdg_wm_base_create_positioner(rf_g_xdg_wm_base.global);
	xdg_positioner_set_size(positioner, 1, 1);
	xdg_positioner_set_anchor_rect(positioner, 0, 0, 1, 1);

	xdg_surface_get_popup(foo_xdg_surface, bar_xdg_surface, positioner);
	xdg_surface_get_popup(bar_xdg_surface, foo_xdg_surface, positioner);

	rf_ctx_expect_error();

	wl_display_roundtrip(rf_ctx.wl_display);
}

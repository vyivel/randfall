#include <wayland-client-core.h>
#include <wayland-client-protocol.h>

#include "randfall.h"
#include "xdg-shell-protocol.h"

struct toplevel {
	struct rf_xdg_toplevel base;
	bool mapped;
};

static void xdg_surface_handle_configure(
		void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	struct toplevel *toplevel = data;
	if (toplevel->mapped) {
		return;
	}
	xdg_surface_ack_configure(xdg_surface, serial);
	struct rf_buffer *buffer = rf_buffer_create(1, 1);
	wl_surface_attach(toplevel->base.wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_commit(toplevel->base.wl_surface);
	toplevel->mapped = true;
}

static const struct xdg_surface_listener xdg_surface_listener = {
	.configure = xdg_surface_handle_configure,
};

int main(void) {
	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_shm.base, 1);
	rf_global_ensure(&rf_g_wl_compositor.base, 1);
	rf_global_ensure(&rf_g_wl_subcompositor.base, 1);
	rf_global_ensure(&rf_g_xdg_wm_base.base, 1);

	rf_global_collect_all();

	struct toplevel parent = {0};
	rf_xdg_toplevel_init(&parent.base, &xdg_surface_listener, NULL, &parent);

	wl_surface_commit(parent.base.wl_surface);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (parent.mapped) {
			break;
		}
	}

	struct wl_surface *child = wl_compositor_create_surface(rf_g_wl_compositor.global);
	struct wl_subsurface *child_subsurface = wl_subcompositor_get_subsurface(
			rf_g_wl_subcompositor.global, child, parent.base.wl_surface);
	wl_subsurface_set_desync(child_subsurface);

	struct rf_buffer *child_buffer = rf_buffer_create(1, 1);
	wl_surface_attach(child, child_buffer->wl_buffer, 0, 0);
	wl_surface_commit(child);

	wl_display_roundtrip(rf_ctx.wl_display);
}

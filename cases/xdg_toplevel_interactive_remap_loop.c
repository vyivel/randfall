#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <wayland-client-core.h>
#include <wayland-client-protocol.h>

#include "randfall.h"
#include "xdg-shell-protocol.h"

struct toplevel {
	struct rf_xdg_toplevel base;
	bool unmapping;
};

static void buffer_handle_release(void *data, struct wl_buffer *buffer) {
	wl_buffer_destroy(buffer);
}

static const struct wl_buffer_listener buffer_listener = {
	.release = buffer_handle_release,
};

static void sync_callback_handle_done(
		void *data, struct wl_callback *callback, uint32_t callback_data) {
	struct toplevel *toplevel = data;
	toplevel->unmapping = false;

	printf("Finished unmapping\n");

	// Reinitialize
	wl_surface_commit(toplevel->base.wl_surface);
}

static const struct wl_callback_listener sync_callback_listener = {
	.done = sync_callback_handle_done,
};

static void xdg_surface_handle_configure(
		void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	struct toplevel *toplevel = data;
	if (toplevel->unmapping) {
		printf("Ignoring configure %" PRIu32 "\n", serial);
		return;
	}

	xdg_surface_ack_configure(xdg_surface, serial);

	printf("Acked configure %" PRIu32 ", mapping and starting unmapping\n", serial);

	// Map
	struct wl_buffer *buffer = wp_single_pixel_buffer_manager_v1_create_u32_rgba_buffer(
			rf_g_wp_single_pixel_buffer_manager_v1.global, 0, 0, 0, 0);
	wl_buffer_add_listener(buffer, &buffer_listener, NULL);

	wl_surface_attach(toplevel->base.wl_surface, buffer, 0, 0);
	wl_surface_commit(toplevel->base.wl_surface);

	// Request another configure for a good measure
	xdg_toplevel_set_maximized(toplevel->base.xdg_toplevel);
	wl_display_flush(rf_ctx.wl_display);
	struct timespec timespec = {
		.tv_sec = 0,
		.tv_nsec = 200000000,
	};
	nanosleep(&timespec, NULL);

	// Unmap
	wl_surface_attach(toplevel->base.wl_surface, NULL, 0, 0);
	wl_surface_commit(toplevel->base.wl_surface);

	toplevel->unmapping = true;

	struct wl_callback *sync_callback = wl_display_sync(rf_ctx.wl_display);
	wl_callback_add_listener(sync_callback, &sync_callback_listener, toplevel);
}

static const struct xdg_surface_listener xdg_surface_listener = {
	.configure = xdg_surface_handle_configure,
};

int main(void) {
	rf_ctx_start();

	rf_global_ensure(&rf_g_wp_single_pixel_buffer_manager_v1.base, 1);
	rf_global_ensure(&rf_g_wl_compositor.base, 1);
	rf_global_ensure(&rf_g_xdg_wm_base.base, 1);

	rf_global_collect_all();

	rf_xdg_wm_base_add_ponger();

	struct toplevel toplevel = {0};
	rf_xdg_toplevel_init(&toplevel.base, &xdg_surface_listener, NULL, &toplevel);
	wl_surface_commit(toplevel.base.wl_surface);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		// Do nothing
	}
}

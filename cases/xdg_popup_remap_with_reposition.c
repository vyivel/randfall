#include <stdio.h>

#include "randfall.h"

struct toplevel {
	struct rf_xdg_toplevel base;
	bool mapped;
};

struct popup {
	struct rf_xdg_popup base;
	bool mapped;
};

static void toplevel_xdg_surface_handle_configure(
		void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	struct toplevel *toplevel = data;
	if (toplevel->mapped) {
		return;
	}
	xdg_surface_ack_configure(xdg_surface, serial);
	struct rf_buffer *buffer = rf_buffer_create(100, 100);
	rf_buffer_fill(buffer, 0xFF, 0xFF, 0xFF);
	wl_surface_attach(toplevel->base.wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_commit(toplevel->base.wl_surface);
	toplevel->mapped = true;
}

static const struct xdg_surface_listener toplevel_xdg_surface_listener = {
	.configure = toplevel_xdg_surface_handle_configure,
};

static void popup_xdg_surface_handle_configure(
		void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	struct popup *popup = data;
	if (popup->mapped) {
		return;
	}
	xdg_surface_ack_configure(xdg_surface, serial);
	struct rf_buffer *buffer = rf_buffer_create(50, 50);
	rf_buffer_fill(buffer, 0x00, 0x00, 0xFF);
	wl_surface_attach(popup->base.wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_commit(popup->base.wl_surface);
	popup->mapped = true;
}

static const struct xdg_surface_listener popup_xdg_surface_listener = {
	.configure = popup_xdg_surface_handle_configure,
};

static void popup_xdg_popup_handle_popup_done(void *data, struct xdg_popup *xdg_popup) {
	printf("Popup dismissed\n");
}

static const struct xdg_popup_listener popup_xdg_popup_listener = {
	.popup_done = popup_xdg_popup_handle_popup_done,
};

int main(void) {
	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_shm.base, 1);
	rf_global_ensure(&rf_g_wl_compositor.base, 1);
	rf_global_ensure(&rf_g_xdg_wm_base.base, 3);

	rf_global_collect_all();

	struct toplevel toplevel = {0};
	rf_xdg_toplevel_init(&toplevel.base, &toplevel_xdg_surface_listener, NULL, &toplevel);
	wl_surface_commit(toplevel.base.wl_surface);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (toplevel.mapped) {
			break;
		}
	}

	struct popup popup = {0};
	rf_xdg_popup_init(&popup.base, toplevel.base.xdg_surface, NULL, &popup_xdg_surface_listener,
			&popup_xdg_popup_listener, &popup);
	wl_surface_commit(popup.base.wl_surface);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (popup.mapped) {
			break;
		}
	}

	wl_surface_attach(popup.base.wl_surface, NULL, 0, 0);
	wl_surface_commit(popup.base.wl_surface);
	popup.mapped = false;

	struct xdg_positioner *positioner = xdg_wm_base_create_positioner(rf_g_xdg_wm_base.global);
	xdg_positioner_set_size(positioner, 2, 2);
	xdg_positioner_set_anchor_rect(positioner, 0, 0, 1, 1);

	xdg_popup_reposition(popup.base.xdg_popup, positioner, 1337);

	// Initial commit
	wl_surface_commit(popup.base.wl_surface);

	printf("Waiting for the popup to be re-mapped\n");

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (popup.mapped) {
			break;
		}
	}
}

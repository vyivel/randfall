#include <wayland-client-core.h>
#include <wayland-client-protocol.h>

#include "randfall.h"
#include "xdg-shell-protocol.h"

struct toplevel {
	struct rf_xdg_toplevel base;
	bool mapped;
};

static void xdg_surface_handle_configure(
		void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	struct toplevel *toplevel = data;
	if (toplevel->mapped) {
		return;
	}
	xdg_surface_ack_configure(xdg_surface, serial);
	struct rf_buffer *buffer = rf_buffer_create(1, 1);
	wl_surface_attach(toplevel->base.wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_commit(toplevel->base.wl_surface);
	toplevel->mapped = true;
}

static const struct xdg_surface_listener xdg_surface_listener = {
	.configure = xdg_surface_handle_configure,
};

int main(void) {
	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_shm.base, 1);
	rf_global_ensure(&rf_g_wl_compositor.base, 1);
	rf_global_ensure(&rf_g_xdg_wm_base.base, 1);

	rf_global_collect_all();

	struct toplevel foo = {0}, bar = {0};
	rf_xdg_toplevel_init(&foo.base, &xdg_surface_listener, NULL, &foo);
	rf_xdg_toplevel_init(&bar.base, &xdg_surface_listener, NULL, &bar);

	wl_surface_commit(foo.base.wl_surface);
	wl_surface_commit(bar.base.wl_surface);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (foo.mapped && bar.mapped) {
			break;
		}
	}

	xdg_toplevel_set_parent(bar.base.xdg_toplevel, foo.base.xdg_toplevel);
	xdg_toplevel_set_parent(foo.base.xdg_toplevel, bar.base.xdg_toplevel);

	rf_ctx_expect_error();

	wl_display_roundtrip(rf_ctx.wl_display);
}

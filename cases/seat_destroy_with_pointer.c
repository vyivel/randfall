#include <wayland-client-protocol.h>

#include "randfall.h"
#include "xdg-shell-protocol.h"

struct toplevel {
	struct rf_xdg_toplevel base;
	bool mapped;
	bool closed;
};

struct pointer {
	struct rf_wl_pointer base;

	struct toplevel *toplevel;
};

static void wl_seat_handle_capabilities(
		void *data, struct wl_seat *wl_seat, uint32_t capabilities) {
	uint32_t *out = data;
	*out = capabilities;
}

static void wl_seat_handle_name(void *data, struct wl_seat *wl_seat, const char *name) {
	// Ignored
}

static const struct wl_seat_listener wl_seat_listener = {
	.capabilities = wl_seat_handle_capabilities,
	.name = wl_seat_handle_name,
};

static void redraw(struct toplevel *toplevel, bool entered) {
	struct rf_buffer *buffer = rf_buffer_create(400, 400);
	if (entered) {
		rf_buffer_fill(buffer, 0xFF, 0x00, 0x00);
	} else {
		rf_buffer_fill(buffer, 0xFF, 0xFF, 0xFF);
	}
	wl_surface_attach(toplevel->base.wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_damage(toplevel->base.wl_surface, 0, 0, INT32_MAX, INT32_MAX);
	wl_surface_commit(toplevel->base.wl_surface);
}

static void wl_pointer_handle_enter(void *data, struct wl_pointer *wl_pointer, uint32_t serial,
		struct wl_surface *surface, wl_fixed_t x, wl_fixed_t y) {
	struct pointer *pointer = data;
	redraw(pointer->toplevel, true);
}

static void wl_pointer_handle_leave(
		void *data, struct wl_pointer *wl_pointer, uint32_t serial, struct wl_surface *surface) {
	struct pointer *pointer = data;
	redraw(pointer->toplevel, false);
}

static const struct wl_pointer_listener wl_pointer_listener = {
	.enter = wl_pointer_handle_enter,
	.leave = wl_pointer_handle_leave,
};

static void toplevel_xdg_surface_handle_configure(
		void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	struct toplevel *toplevel = data;
	if (toplevel->mapped) {
		return;
	}
	xdg_surface_ack_configure(xdg_surface, serial);
	redraw(toplevel, false);
	toplevel->mapped = true;
}

static const struct xdg_surface_listener toplevel_xdg_surface_listener = {
	.configure = toplevel_xdg_surface_handle_configure,
};

static void toplevel_xdg_toplevel_handle_close(void *data, struct xdg_toplevel *xdg_toplevel) {
	struct toplevel *toplevel = data;
	toplevel->closed = true;
}

static const struct xdg_toplevel_listener toplevel_xdg_toplevel_listener = {
	.close = toplevel_xdg_toplevel_handle_close,
};

int main(void) {
	RF_GLOBAL_DEFINE(wl_seat, g_seat);

	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_shm.base, 1);
	rf_global_ensure(&rf_g_wl_compositor.base, 1);
	rf_global_ensure(&rf_g_xdg_wm_base.base, 1);

	rf_global_ensure(&g_seat.base, 5);

	rf_global_collect_all();

	rf_xdg_wm_base_add_ponger();

	uint32_t capabilities = 0;
	wl_seat_add_listener(g_seat.global, &wl_seat_listener, &capabilities);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if ((capabilities & WL_SEAT_CAPABILITY_POINTER) != 0) {
			break;
		}
	}

	struct toplevel toplevel = {0};
	rf_xdg_toplevel_init(&toplevel.base, &toplevel_xdg_surface_listener,
			&toplevel_xdg_toplevel_listener, &toplevel);
	wl_surface_commit(toplevel.base.wl_surface);

	struct pointer pointer = {
		.toplevel = &toplevel,
	};
	rf_wl_pointer_init(&pointer.base, g_seat.global, &wl_pointer_listener, &pointer);

	wl_seat_release(g_seat.global);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (toplevel.closed) {
			break;
		}
	}
}

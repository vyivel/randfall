#include <wayland-client-core.h>
#include <wayland-client-protocol.h>

#include "randfall.h"
#include "xdg-shell-protocol.h"

struct toplevel {
	struct rf_xdg_toplevel base;
	bool configured;
	uint32_t configure_serial;
};

static void xdg_surface_handle_configure(
		void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	struct toplevel *toplevel = data;
	toplevel->configured = true;
	toplevel->configure_serial = serial;
}

static const struct xdg_surface_listener xdg_surface_listener = {
	.configure = xdg_surface_handle_configure,
};

int main(void) {
	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_shm.base, 1);
	rf_global_ensure(&rf_g_wl_compositor.base, 1);
	rf_global_ensure(&rf_g_xdg_wm_base.base, 1);

	rf_global_collect_all();

	struct toplevel toplevel = {0};
	rf_xdg_toplevel_init(&toplevel.base, &xdg_surface_listener, NULL, &toplevel);

	wl_surface_commit(toplevel.base.wl_surface);

	toplevel.configured = false;
	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (toplevel.configured) {
			break;
		}
	}

	struct rf_buffer *buffer1 = rf_buffer_create(1, 1);
	wl_surface_attach(toplevel.base.wl_surface, buffer1->wl_buffer, 0, 0);
	wl_surface_commit(toplevel.base.wl_surface);

	// The compositor has to respond with a configure event to this
	xdg_toplevel_set_fullscreen(toplevel.base.xdg_toplevel, NULL);

	toplevel.configured = false;
	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (toplevel.configured) {
			break;
		}
	}

	wl_surface_attach(toplevel.base.wl_surface, NULL, 0, 0);
	wl_surface_commit(toplevel.base.wl_surface);

	xdg_surface_ack_configure(toplevel.base.xdg_surface, toplevel.configure_serial);

	struct rf_buffer *buffer2 = rf_buffer_create(1, 1);
	wl_surface_attach(toplevel.base.wl_surface, buffer2->wl_buffer, 0, 0);
	wl_surface_commit(toplevel.base.wl_surface);

	rf_ctx_expect_error();

	wl_display_roundtrip(rf_ctx.wl_display);
}

#include <linux/input-event-codes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wayland-client-protocol.h>

#include "randfall.h"
#include "xdg-shell-protocol.h"

struct toplevel {
	struct rf_xdg_toplevel base;
	bool mapped;
	bool closed;
};

struct popup {
	struct rf_xdg_popup base;
	bool opened;
	bool mapped;
	bool waiting_for_button;
};

static struct toplevel toplevel = {0};

static struct popup popup = {0};

static struct wl_data_device *wl_data_device = NULL;

static struct wl_surface *entered_surface = NULL;

static RF_GLOBAL_DEFINE(wl_seat, g_seat);

static void toplevel_xdg_surface_handle_configure(
		void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	if (toplevel.mapped) {
		return;
	}
	xdg_surface_ack_configure(xdg_surface, serial);
	struct rf_buffer *buffer = rf_buffer_create(100, 100);
	rf_buffer_fill(buffer, 0xFF, 0xFF, 0xFF);
	wl_surface_attach(toplevel.base.wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_commit(toplevel.base.wl_surface);
	toplevel.mapped = true;
}

static const struct xdg_surface_listener toplevel_xdg_surface_listener = {
	.configure = toplevel_xdg_surface_handle_configure,
};

static void toplevel_xdg_toplevel_handle_close(void *data, struct xdg_toplevel *xdg_toplevel) {
	struct toplevel *toplevel = data;
	toplevel->closed = true;
}

static const struct xdg_toplevel_listener toplevel_xdg_toplevel_listener = {
	.close = toplevel_xdg_toplevel_handle_close,
};

static void popup_xdg_surface_handle_configure(
		void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	if (popup.mapped) {
		return;
	}
	xdg_surface_ack_configure(xdg_surface, serial);
	struct rf_buffer *buffer = rf_buffer_create(100, 100);
	rf_buffer_fill(buffer, 0x00, 0x00, 0xFF);
	wl_surface_attach(popup.base.wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_commit(popup.base.wl_surface);
	popup.mapped = true;
}

static const struct xdg_surface_listener popup_xdg_surface_listener = {
	.configure = popup_xdg_surface_handle_configure,
};

static void popup_xdg_popup_handle_popup_done(void *data, struct xdg_popup *xdg_popup) {
	if (entered_surface == popup.base.wl_surface) {
		entered_surface = NULL;
	}
	xdg_popup_destroy(popup.base.xdg_popup);
	xdg_surface_destroy(popup.base.xdg_surface);
	wl_surface_destroy(popup.base.wl_surface);
	popup = (struct popup){0};
	printf("Popup dismissed\n");
}

static const struct xdg_popup_listener popup_xdg_popup_listener = {
	.popup_done = popup_xdg_popup_handle_popup_done,
};

static void wl_data_source_handle_target(
		void *data, struct wl_data_source *wl_data_source, const char *mime_type) {
	// No-op
}

static void wl_data_source_handle_send(
		void *data, struct wl_data_source *wl_data_source, const char *mime_type, int32_t fd) {
	// No-op
}

static void wl_data_source_handle_cancelled(void *data, struct wl_data_source *wl_data_source) {
	printf("Drag-and-drop operation cancelled\n");
}

static void wl_data_source_handle_dnd_drop_performed(
		void *data, struct wl_data_source *wl_data_source) {
	printf("Drag-and-drop drop performed\n");
}

static void wl_data_source_handle_dnd_finished(void *data, struct wl_data_source *wl_data_source) {
	printf("Drag-and-drop finished\n");
}

static void wl_data_source_handle_action(
		void *data, struct wl_data_source *wl_data_source, uint32_t action) {
	// No-op
}

static const struct wl_data_source_listener wl_data_source_listener = {
	.target = wl_data_source_handle_target,
	.send = wl_data_source_handle_send,
	.cancelled = wl_data_source_handle_cancelled,
	.dnd_drop_performed = wl_data_source_handle_dnd_drop_performed,
	.dnd_finished = wl_data_source_handle_dnd_finished,
	.action = wl_data_source_handle_action,
};

static void create_drag(uint32_t serial) {
	struct wl_data_source *wl_data_source =
			wl_data_device_manager_create_data_source(rf_g_wl_data_device_manager.global);
	wl_data_source_add_listener(wl_data_source, &wl_data_source_listener, NULL);

	wl_data_source_set_actions(wl_data_source, WL_DATA_DEVICE_MANAGER_DND_ACTION_COPY);

	struct wl_surface *icon = wl_compositor_create_surface(rf_g_wl_compositor.global);
	wl_data_device_start_drag(wl_data_device, wl_data_source, entered_surface, icon, serial);
	struct rf_buffer *icon_buffer = rf_buffer_create(30, 30);
	rf_buffer_fill(icon_buffer, 0xFF, 0x00, 0x00);
	wl_surface_attach(icon, icon_buffer->wl_buffer, 0, 0);
	wl_surface_commit(icon);
}

static void create_popup(uint32_t serial) {
	struct xdg_positioner *positioner = xdg_wm_base_create_positioner(rf_g_xdg_wm_base.global);
	xdg_positioner_set_size(positioner, 1, 1);
	xdg_positioner_set_anchor_rect(positioner, 0, 0, 1, 1);
	xdg_positioner_set_offset(positioner, 80, 80);

	rf_xdg_popup_init(&popup.base, toplevel.base.xdg_surface, positioner,
			&popup_xdg_surface_listener, &popup_xdg_popup_listener, NULL);

	xdg_popup_grab(popup.base.xdg_popup, g_seat.global, serial);
	wl_surface_commit(popup.base.wl_surface);

	popup.opened = true;
}

static void wl_seat_handle_capabilities(
		void *data, struct wl_seat *wl_seat, uint32_t capabilities) {
	uint32_t *out = data;
	*out = capabilities;
}

static const struct wl_seat_listener wl_seat_listener = {
	.capabilities = wl_seat_handle_capabilities,
};

static void wl_pointer_handle_enter(void *data, struct wl_pointer *wl_pointer, uint32_t serial,
		struct wl_surface *surface, wl_fixed_t surface_x, wl_fixed_t surface_y) {
	entered_surface = surface;
}

static void wl_pointer_handle_leave(
		void *data, struct wl_pointer *wl_pointer, uint32_t serial, struct wl_surface *surface) {
	entered_surface = NULL;
}

static void wl_pointer_handle_button(void *data, struct wl_pointer *wl_pointer, uint32_t serial,
		uint32_t time, uint32_t button, uint32_t state) {
	if (state != WL_POINTER_BUTTON_STATE_PRESSED) {
		return;
	}

	if (entered_surface == toplevel.base.wl_surface) {
		if (popup.opened) {
			return;
		}

		printf("---\n");

		switch (button) {
		case BTN_LEFT:
			printf("Creating a grabbing xdg_popup and starting a drag-and-drop operation\n");
			create_popup(serial);
			create_drag(serial);
			break;
		case BTN_MIDDLE:
			printf("Starting a drag-and-drop operation and creating a grabbing xdg_popup\n");
			create_drag(serial);
			create_popup(serial);
			break;
		case BTN_RIGHT:
			printf("Creating a grabbing xdg_popup and waiting for wl_pointer.button on "
				   "the xdg_popup\n");
			create_popup(serial);
			popup.waiting_for_button = true;
			break;
		default:
			break;
		}
	} else if (popup.opened && popup.waiting_for_button &&
			entered_surface == popup.base.wl_surface) {
		printf("Starting a drag-and-drop operation\n");
		create_drag(serial);
	}
}

static const struct wl_pointer_listener wl_pointer_listener = {
	.enter = wl_pointer_handle_enter,
	.leave = wl_pointer_handle_leave,
	.button = wl_pointer_handle_button,
};

int main(void) {
	rf_ctx_start();

	rf_global_ensure(&rf_g_wl_shm.base, 1);
	rf_global_ensure(&rf_g_wl_compositor.base, 1);
	rf_global_ensure(&rf_g_xdg_wm_base.base, 1);
	rf_global_ensure(&rf_g_wl_data_device_manager.base, 3);

	rf_global_ensure(&g_seat.base, 1);

	rf_global_collect_all();

	rf_xdg_wm_base_add_ponger();

	uint32_t capabilities = 0;
	wl_seat_add_listener(g_seat.global, &wl_seat_listener, &capabilities);

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if ((capabilities & WL_SEAT_CAPABILITY_POINTER) != 0) {
			break;
		}
	}

	wl_data_device = wl_data_device_manager_get_data_device(
			rf_g_wl_data_device_manager.global, g_seat.global);

	struct rf_wl_pointer pointer;
	rf_wl_pointer_init(&pointer, g_seat.global, &wl_pointer_listener, NULL);

	rf_xdg_toplevel_init(
			&toplevel.base, &toplevel_xdg_surface_listener, &toplevel_xdg_toplevel_listener, NULL);
	wl_surface_commit(toplevel.base.wl_surface);

	printf("Left mouse button: xdg_popup.grab followed by wl_data_device.start_drag\n");
	printf("Middle mouse button: wl_data_device.start_drag followed by xdg_popup.grab\n");
	printf("Right mouse button: xdg_popup.grab, wl_data_device.start_drag from xdg_popup\n");

	while (wl_display_dispatch(rf_ctx.wl_display) != -1) {
		if (toplevel.closed) {
			break;
		}
	}
}
